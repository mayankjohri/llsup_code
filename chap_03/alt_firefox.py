#!/usr/bin/env python
# coding=utf-8
# alt_firefox.py
"""."""

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


firefox_path = '~/Applications/Firefox Developer Edition.app/Contents/MacOS/firefox-bin'
BIN = FirefoxBinary(firefox_path)
driver = webdriver.Firefox(firefox_binary=BIN)
driver.get("http://127.0.0.1:5000/")
ele = driver.find_element_by_class_name("Y1800")
print(ele.text)
driver.close()
