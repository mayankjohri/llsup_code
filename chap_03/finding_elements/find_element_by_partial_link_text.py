#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver


with webdriver.Firefox() as driver:
  driver.get("http://127.0.0.1:5000/")
  ele = driver.find_element_by_partial_link_text("Hash")
  print(ele.text)
