#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep


with webdriver.Firefox() as driver:
  driver.get("http://127.0.0.1:5000/")
  ele = driver.find_element(By.NAME, "option_two")
  sleep(5)
  print(dir(ele))
  print(ele.text)
