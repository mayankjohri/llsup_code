#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.common.by import By


with webdriver.Firefox() as driver:
  driver.get("http://127.0.0.1:5000/")
  ele = driver.find_element(By.LINK_TEXT, "Test Hash")
  print(ele.text)
