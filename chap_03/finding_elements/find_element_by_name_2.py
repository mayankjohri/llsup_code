#!/usr/bin/env python
# coding=utf-8
# chap_03/finding_elements/find_elements_by_name.py
from selenium import webdriver
from selenium.webdriver.common.by import By


with webdriver.Firefox() as driver:
  driver.get("http://127.0.0.1:5000/")
  ele = driver.find_element(By.NAME, "optionsRadios")
  print(ele.text)
  print(ele.get_attribute("value"))
