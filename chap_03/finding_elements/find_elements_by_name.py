#!/usr/bin/env python
# coding=utf-8
# chap_03/finding_elements/find_elements_by_name.py
from selenium import webdriver


with webdriver.Firefox() as driver:
  driver.get("http://127.0.0.1:5000/")
  ele = driver.find_elements_by_name("optionsRadios")
  for element in ele:
    parent = element.find_element_by_xpath("..")
    print(parent.text)
