#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.common.by import By


with webdriver.Firefox() as driver:
  driver.get("http://127.0.0.1:5000/")
  # This will not work as id has invalid name
  ele = driver.find_element(By.ID, "button_basic")
  print(ele.text)
