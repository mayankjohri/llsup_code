#!/usr/bin/env python
# coding=utf-8


from selenium import webdriver
from selenium.webdriver.common.by import By 

with webdriver.Firefox() as driver:
  driver.get("http://127.0.0.1:5000/")
  css_apple = 'table[class~="table"][class~="table-dark"] tbody tr'
  ele = driver.find_element(By.CSS_SELECTOR, css_apple)
  print(dir(ele))
  print(ele.text)
  row = ele.find_elements(By.CSS_SELECTOR, "td")[2]
  print(row.text)
  # for x in ele.find_elements(By.CSS_SELECTOR, "td"):
  #   print(x.text)

