#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/")
    selector = 'input[type=\'radio\']:checked'
    elements = driver.find_elements_by_css_selector(selector)
    for ele in elements:
        print(ele.text)
