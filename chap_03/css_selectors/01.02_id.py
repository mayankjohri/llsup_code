#!/usr/bin/env python
# coding=utf-8


from selenium import webdriver
from selenium.webdriver.common.by import By 

with webdriver.Firefox() as driver:
  driver.get("http://127.0.0.1:5000/")
  css_apple = 'table[class~="table"][class~="table-dark"] tbody'
  ele = driver.find_element(By.CSS_SELECTOR, css_apple)
  print(dir(ele))
  print(ele.text)
  row = ele.find_elements(By.CSS_SELECTOR, "tr")[2]
  print(f" {row.text = }")
  print("row is ", ele.find_element(By.CSS_SELECTOR, "tr:nth-child(2)").text)
  for x in row.find_elements(By.CSS_SELECTOR, "td"):
     print(f" {x.text = } ")

