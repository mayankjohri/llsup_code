#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver

hub_url = "http://127.0.0.1:4545/wd/hub"

desired_capabilities={
  "browserName":"chrome"
}

with webdriver.Remote(hub_url, desired_capabilities) as driver:
    driver.get("http://127.0.0.1:5000/")
    ele = driver.find_element_by_name("option_two")
    print(ele.text)
