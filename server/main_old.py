#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 15:57:43 2018.

@author: mayank
"""
from flask import Flask, render_template, redirect, url_for, request


app = Flask(__name__)


@app.route("/namaskar")
def namaskar():
    return render_template("namaskar.html")

@app.route("/nested")
def nested():
    return render_template("nested_elements.html")


@app.route("/imgs")
def images():
    return render_template("images.html")


@app.route("/ramram")
def ram_ram():
    return render_template("console_ramram.html")


@app.route("/timeout")
def timeout_text():
    return render_template("timeout.html")


@app.route("/links")
def links():
    """."""
    return render_template('links.html')


@app.route("/frame")
def frame_example():
    """."""
    return render_template('frames_example.html')


@app.route("/iframe")
def iframe_example():
    """."""
    return render_template('iframe_example.html')


@app.route("/bulma")
def bulma():
    """."""
    return render_template('bulma.html')


@app.route("/popups")
def popups():
    """."""
    return render_template('popups.html')


@app.route("/alert")
def alert():
    """."""
    return render_template('alert.html')


@app.route("/alert_now")
def alert_now():
    """."""
    return render_template('alert_now.html')


@app.route("/")
def template_test():
    """."""
    return render_template('main.html')


@app.route('/success/')
def success():
    """."""
    return '<html><head><title>Welcome Success</title></head>' \
        + '<body><p id="success">!!! Welcome Success !!!</p></body>'


@app.route("/send_clear")
def template_send_clear():
    """."""
    return render_template('send_clear.html')


@app.route("/form_1", methods=['POST', 'GET'])
def template_form_1():
    """."""
    if request.method == 'GET':
        return render_template('form_submit.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/form_2", methods=['POST', 'GET'])
def template_form_2():
    """."""
    if request.method == 'GET':
        return render_template('form_bootstrap.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/form_bootstrap_dropdown", methods=['POST', 'GET'])
@app.route("/bootstrap_dropdown", methods=['POST', 'GET'])
def bootstrap_dropdown():
    """."""
    if request.method == 'GET':
        return render_template('form_bootstrap_dropdown.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/form_bootstrap_radiobutton", methods=['POST', 'GET'])
@app.route("/bootstrap_radiobutton", methods=['POST', 'GET'])
def bootstrap_radiobutton():
    """."""
    if request.method == 'GET':
        return render_template('form_bootstrap_radiobutton.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/form_cb_badge", methods=['POST', 'GET'])
def form_cb():
    """."""
    if request.method == 'GET':
        return render_template('form_bootstrap_checkbox_badge.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/form_bootstrap_progress", methods=['POST', 'GET'])
@app.route("/bootstrap_progress", methods=['POST', 'GET'])
def form_bootstrap_progress():
    """."""
    if request.method == 'GET':
        return render_template('form_bootstrap_progress.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/form_html5_progress", methods=['POST', 'GET'])
@app.route("/html5_progress", methods=['POST', 'GET'])
def form_html5_progress():
    """."""
    if request.method == 'GET':
        return render_template('form_html5_progress.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/bootstrap_menu", methods=['POST', 'GET'])
def bootstrap_menu():
    """."""
    if request.method == 'GET':
        return render_template('bootstrap_menu.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/canvas", methods=['POST', 'GET'])
def canvas():
    """."""
    if request.method == 'GET':
        return render_template('canvas.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/drag_and_drop", methods=['POST', 'GET'])
def drag_and_drop():
    """."""
    if request.method == 'GET':
        return render_template('drag_and_drop.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/cookies", methods=['POST', 'GET'])
def cookies():
    """."""
    if request.method == 'GET':
        return render_template('cookies.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/cd1", methods=['POST', 'GET'])
def countdown_v1():
    """."""
    if request.method == 'GET':
        return render_template('countdown_1.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


@app.route("/countdown", methods=['POST', 'GET'])
def countdown():
    """."""
    if request.method == 'GET':
        return render_template('countdown.html')
    elif request.method == 'POST':
        return redirect(url_for('success'))


if __name__ == '__main__':
    app.run(debug=True)
