# Automation Testing - The Missing FAQ's
------------
[TOC]


## Basics of Automation

### Q. Is it feasible to have 100% automation?
No, we can not achieve 100% automation and there are to many reasons, few of them are as follows
- Cost of automation: There are few test conditions which are very rare, such as admin tab in a chat application which only get update once in a year and has only one page, thus automating it will not be of any benefit.
- Rare boundary value test condition

### Q. What is the characteristic of good automation framework
The good automation framework should have the following characteristics
- consistent & accurate: The tests results should be consistent & accurate, they should not produce different results for subsequent executions with no  apparent change in test environment.
- Modular: Change in one test module should not introduce issues in another module.
- Reusable: The framework should be flexible enough that it can be used in multiple projects without significant changes.
- independent test cases: Test cases should be nuclear in nature and should not need other test-cases for its execution. Exception can be login module.
- fast execution: The test cases should not take too long to run else they will never be executed.
- multi threading and multi node execution
- Summary & Extensive Reporting
-  Integration

### Q. Which test cases are good candidates for automation?
Following type of test-cases are good candidates for automation. The test-cases which return most ROI should be the first candidate for automation.

ROI can be calculated based on

- how much time it will take to create the test-cases and how often they will be used
- Test cases for modules which are crucial for the application
- Test case executed with different set of data
- Test case executed with different browser
- Test case executed with different environment
- Test case executed with complex business logic
- Test case executed with different set of users
- Test case Involves large amount of data
- Test case has any dependency
- Test case requires Special data
- Smoke/Sanity/Regression Test cases

### Q. Where you maintain information like URL, login, password?
The information which often change should not be part of the automation code but should be stored in separate location, such as database or configuration files (yaml, csv, ini files). They can also be clubbed in groups say for details of staging server, production server etc.


### Q. Why do you want to keep this kind of information in separate file and not directly in code?
URL, Login and Password are the kind of fields which are used very often and these changes as per the environment and authorization. In case we hard coded it into our code, we have to change it in every file which has its reference. In case there are say more than 100 files, then it becomes very difficult to change in all the 100 files and hence can lead to errors. So this kind of information is maintained in a separate file so that updating becomes easy.

### Q. Can you tell some good coding practices while automation?
- Proper comments, they should be at the
	* start of test module informing which module it is testing.
	* start of test-case: informing about the test-case, story, what is being tested and what is the expected output
- logging at proper location.
- keep the test-cases small
- let the test-case test one and only one condition
- keep the hard-coding such as URL's, user details etc in configuration file or database.

### Q. How do you select which automation tool is best suited for you?
I would look at the following parameters while selecting automation tool
- Ease of use
- Less creation time
- Less execution time
- Easy Maintainable
- Support
- Custom execution: Should have to ability to select test-cases to run at runtime
- Time to learn and implement the tool

###Q. What is a framework?
Automation framework is the main structure of any automation solution. It consists of many components such as
- Reporting
- Test Cases / Test case Metadata
- Test Data
- Environment Data
- Execution History

### Q. How do you decide whether to go for manual testing or automation testing?
It depends on the ROI, which in turns depends on
- Frequency of testcase execution
- Life cycle of the application
- Time taken for automation

### Q. What approach would you take if the automated tests take too long to run?
- Break the test-case in multiple test-cases and then execute them. 
- Also we can weigh if approach of the testcase can be changed, say for example for UI based to backend based.





# Testing Framework

## JUnit / TestNG
### Q. Are using JUnit/TestNG for test cases?
Yes

### Q. How to set test case priority in TestNG?
There are multiple ways to set the case priority.
1.  inside Annotation text
@Test(priority=0), @Test(priority=1), @Test(priority=2)

### Q. How to make sure when using priority on testcases in multiple classes do not combine. i.e. testcases should club based on files also instead of priority alone.

In suite.xml use group-by-instances="true".
```xml
<test verbose="2" name="NewTestGroup" group-by-instances="true">
```
It will make sure taht the code is executed priority sorted per class.


### Q. TestNG Annotations
Following are the TestNG Annotations
- BeforeSuite
- AfterSuite
- BeforeTest
- AfterTest
- BeforeClass
- AfterClass
- BeforeMethod
- AfterMethod
- Test

### Q. What is the difference between `@BeforeMethod` and `@BeforeTest`

| `@BeforeTest` 	| `@BeforeMethod` |
|---------------|---------------|
| It executes before any of the test methods included in the `<test>` tag in the `testng.xml` file. | It executes before any of the test methods annotated as `@Test`. |

### Q. What are the advantages of TestNG?
Most common advantage of TestNG are as follows
1. Multi level pre and post methods such as beforeSuite, AfterSuite, BeforeClass, AfterClass etc.
2. Custom Reporting like: allure framework
3. multi-threading
4. Grouping of test cases
5. Prioritising of test cases.
6. Data driven testcases usign @DataProvider
7. Support for parameterization using @Parameters

### Q. How to pass parameter through testng.xml file to a test case?


### Q. What is TestNG Assert and list out common TestNG Assertions?


### Q. What is the difference between SoftAssert and HardAssert?



### Q. What is the use of AssertAll() in SoftAssert?

## py.test

## SELENIUM

### Q. How to handle frame in WebDriver?

### Q. How to click on a hyper link using linkText?


## CUCUMBER QUESTIONS

## CONTINOUS INTEGRATION (JENKINS)


## Misc

### Q. How can I read test data from excels?
POI and JXL can be used to read excel file
