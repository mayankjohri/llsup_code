from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep

driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/bulma")
text_area = driver.find_element_by_css_selector("textarea[class='textarea']")
text_area.send_keys(Keys.CONTROL + "a")
text_area.send_keys(Keys.CONTROL + "c")
text_area.clear()
sleep(2)
text_area.send_keys(Keys.CONTROL + "v")
sleep(2)
driver.quit()
