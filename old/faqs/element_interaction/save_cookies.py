from selenium import webdriver
import pickle

driver = webdriver.Chrome()
driver.get("http://www.google.com")
with open("google_cookies.pkl", "wb") as pk:
    pickle.dump(driver.get_cookies(), pk)

driver.quit()
