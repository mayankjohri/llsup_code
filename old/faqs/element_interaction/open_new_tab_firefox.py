from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep
import sys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


firefox_path = '/home/mayank/apps/firefox/firefox'
gecko_path = "/home/mayank/apps/web_drivers/geckodriver"

options = Options()
binary = FirefoxBinary(firefox_path,
                       log_file=sys.stdout)

driver = webdriver.Firefox(executable_path=gecko_path,
                           firefox_binary=binary,
                           firefox_options=options)
driver.get("http://127.0.0.1:5000/bulma")

#open tab
driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 't')
sleep(2)
# Load a page
driver.get('http://127.0.0.1:5000/alert')
# Make the tests...
sleep(2)
# close the tab
driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 'w')

sleep(4)
driver.quit()
