from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep

driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/bulma")
text_area = driver.find_element_by_css_selector("textarea[class='textarea']")

#open tab
driver.find_element_by_tag_name('body').send_keys(Keys.COMMAND + 't')
sleep(2)
# Load a page
driver.get('http://127.0.0.1:5000/alert')
# Make the tests...
sleep(2)
# close the tab
driver.find_element_by_tag_name('body').send_keys(Keys.COMMAND + 'w')

sleep(2)
driver.quit()
