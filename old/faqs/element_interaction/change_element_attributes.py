from selenium import webdriver

driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/ramram")
ram = driver.find_element_by_css_selector("h1")

print("Initial:",ram.get_attribute("class"))
driver.execute_script("arguments[0].setAttribute('class','godlike')", ram)
ram = driver.find_element_by_css_selector("h1")

print("after:", ram.get_attribute("class"))
driver.quit()
