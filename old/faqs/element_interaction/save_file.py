import os
from selenium import webdriver

firefox_path = '/home/mayank/apps/firefox/firefox'
gecko_path = "/home/mayank/apps/web_drivers/geckodriver"

profile = webdriver.FirefoxProfile()
profile.set_preference('browser.download.folderList', 2)
profile.set_preference('browser.download.manager.showWhenStarting', False)
profile.set_preference('browser.download.dir', os.getcwd())
profile.set_preference('browser.helperApps.neverAsk.saveToDisk',
                       'text/csv/xls')
driver = webdriver.Firefox(profile,
                           executable_path=gecko_path,
                           firefox_binary=firefox_path)
driver.get("http://127.0.0.1:5000/alert")
driver.find_element_by_id("dummy").click()
driver.quit()
