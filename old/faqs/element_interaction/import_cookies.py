from selenium import webdriver
import pickle

driver = webdriver.Chrome()
driver.get("http://www.google.com")
with open("google_cookies.pkl", "rb") as pk:
    cookies = pickle.load(pk)
    for cookie in cookies:
        driver.add_cookie(cookie)
driver.close()
