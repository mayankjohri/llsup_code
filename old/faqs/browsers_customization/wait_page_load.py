from selenium import webdriver
from selenium.common.exceptions import TimeoutException
import time


def wait_for(condition_function, args):
    start_time = time.time()
    while time.time() < start_time + 3:
        while condition_function(args):
            return True
        else:
            time.sleep(0.2)
    raise Exception(F'Timeout waiting for {condition_function.__name__}')


def page_has_loaded(driver):
        page_state = driver.execute_script(
            'return document.readyState;'
        )
        return page_state == 'complete'


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

try:
    wait_for(page_has_loaded, driver)
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
