from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

firefox_path = '/home/mayank/apps/firefox/firefox'
gecko_path = "/home/mayank/apps/web_drivers/geckodriver"

options = Options()
# options.set_headless(headless=True)
options.add_argument("--headless")
binary = FirefoxBinary(firefox_path,
                       log_file=sys.stdout)

driver = webdriver.Firefox(executable_path=gecko_path,
                           firefox_binary=binary,
                           firefox_options=options)
driver.get("http://google.com/")
search_text = driver.find_element_by_id("lst-ib")
driver.quit()
