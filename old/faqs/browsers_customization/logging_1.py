import logging
from selenium.webdriver.remote.remote_connection import LOGGER
from selenium import webdriver 

LOGGER.setLevel(logging.WARNING)

driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/bulma")
text_area = driver.find_element_by_css_selector("textarea[class='textarea']")
parent = text_area.find_element_by_xpath("..")
print(parent.get_attribute('innerHTML'))

driver.quit()