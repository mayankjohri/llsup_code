import sys
from time import sleep

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


firefox_path = '/home/mayank/apps/firefox/firefox'
gecko_path = "/home/mayank/apps/web_drivers/geckodriver"

options = Options()

profile = webdriver.FirefoxProfile()
# profile.set_preference('browser.window.width', 0)
# profile.set_preference('browser.window.height', 0)
# profile.update_preferences()

binary = FirefoxBinary(firefox_path,
                       log_file=sys.stdout)
try:
    driver = webdriver.Firefox(executable_path=gecko_path,
                               firefox_binary=binary,
                               firefox_options=options,
                               firefox_profile=profile)

    driver.set_window_size(100, 300)
    driver.get("http://127.0.0.1:5000/bulma")
except Exception as e:
    print(e)
finally:
    sleep(2)
    driver.quit()
