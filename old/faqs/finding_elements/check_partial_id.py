from selenium import webdriver
from selenium.webdriver.common.by import By


driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/bulma")

print(driver.find_element(By.CSS_SELECTOR,
                    "textarea[class$='tarea']").text)
print(driver.find_element(By.CSS_SELECTOR,
                    "textarea[class^='textar']").text)
print(driver.find_element(By.CSS_SELECTOR,
                    "textarea[class*='tare']").text)

driver.quit()
