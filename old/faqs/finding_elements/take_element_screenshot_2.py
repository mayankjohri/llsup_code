from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import sys


firefox_path = '/home/mayank/apps/firefox/firefox'
gecko_path = "/home/mayank/apps/web_drivers/geckodriver"

options = Options()
# options.set_headless(headless=True)
options.add_argument("--headless")
binary = FirefoxBinary(firefox_path,
                       log_file=sys.stdout)

driver = webdriver.Firefox(executable_path=gecko_path,
                           firefox_binary=binary,
                           firefox_options=options)
driver.get("http://127.0.0.1:5000/bulma")

textarea = driver.find_element_by_css_selector("textarea")
textarea.screenshot("textarea1.png")

driver.quit()
