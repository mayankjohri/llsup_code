from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep


def set_large_text(driver, element, large_text):
    driver.execute_script("arguments[0].value=arguments[1]",
                          element, large_text)
    # script = "".join(["arguments[0].setAttribute('value', '",
    #                   large_text,
    #                   "');"])
    # print(script)
    # driver.execute_script(script, element)


driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/bulma")

txarea = driver.find_element(By.CSS_SELECTOR,
                             "textarea[class$='tarea']")
print(txarea.get_attribute("value"))
large_text = """Holi is celebrated as the conclusion of winter and the start of spring
to sow the land and hope for a good harvest. This day is marked by
colors and song (Chautal). It does not require specific prayer or
fasting, however some people keep a vegetarian fast on this day.
The Arya Samaj does not associate Holi with a particular deity such as
Vishnu or Shiva and in comparison to some interpretations of the
festival, the Arya Samaj version in more sober and is as per the 4 Vedas"""
set_large_text(driver, txarea, large_text)

sleep(2)

driver.quit()
