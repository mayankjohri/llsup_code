from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/alert_now")

try:
    WebDriverWait(driver, 3).until(EC.alert_is_present(),
                                   'Timed out :(')

    alert = driver.switch_to.alert
    alert.accept()
    print("alert accepted")
except TimeoutException:
    print("no alert")
    

driver.quit()
