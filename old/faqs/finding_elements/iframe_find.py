from selenium import webdriver

firefox_path = '/home/mayank/apps/firefox/firefox'
gecko_path = "/home/mayank/apps/web_drivers/geckodriver"

driver = webdriver.Chrome()

driver.get("http://127.0.0.1:5000/iframe")
driver.switch_to.frame("i_popups")  # Using frame id
# driver.save_screenshot("test.png")
link_1 = driver.find_element_by_id("lnkNewWindow")
print(link_1.get_attribute("target"))
driver.switch_to.default_content()
driver.quit()
