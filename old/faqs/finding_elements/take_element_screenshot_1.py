from selenium import webdriver
from selenium.webdriver.common.by import By


def get_element_screenshot(driver, element, filename):
    from PIL import Image
    from io import BytesIO

    location = element.location
    size = element.size
    png = driver.get_screenshot_as_png()
    im = Image.open(BytesIO(png))

    left, right = location['x'], location['x'] + size['width']
    top, bottom = location['y'], location['y'] + size['height']

    im = im.crop((left, top, right, bottom))
    im.save(filename)


driver = webdriver.Chrome()
driver.get("http://127.0.0.1:5000/bulma")
count = len(driver.find_elements(By.CSS_SELECTOR,
                                 "textarea[class='textarea']")) > 0

textarea = driver.find_element_by_css_selector("textarea")
get_element_screenshot(driver, textarea, "textarea.png")
# NOT Implemented
# textarea.screenshot("/home/mayank/test.png")

driver.quit()
