#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/opt/google/chrome/chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/timeout")

try:
    ele = driver.find_element_by_id("ohm")
    WebDriverWait(driver, 15).until(
        ec.staleness_of(ele)
    )
except TimeoutException as e:
    print("Error: element was still present", e)
finally:
    driver.quit()
