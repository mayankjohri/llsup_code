#!/usr/bin/env python
# coding=utf-8
# wait_title_contains.py
"""."""
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/opt/google/chrome/chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/alert")

try:
    WebDriverWait(driver, 15).until(
        ec.alert_is_present()
    )
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
