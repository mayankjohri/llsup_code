#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/opt/google/chrome/chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/")

try:
    css_val = "#exampleSelect2 > option[value='2']"
    ele = driver.find_element_by_css_selector(css_val)
    WebDriverWait(driver, 15).until(
        ec.element_to_be_selected(ele)
    )
except TimeoutException as e:
    print("Error: element was still present", e)
finally:
    driver.quit()
