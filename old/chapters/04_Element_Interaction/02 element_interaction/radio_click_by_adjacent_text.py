#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://localhost:5000/form_bootstrap_radiobutton")

ele = driver.find_elements_by_css_selector("#opts label")

sel_text = "primary"
for e in ele:
    # print(e)
    s = e.find_element_by_css_selector("span")
    if s.text == sel_text:
        e.find_element_by_css_selector("input[type='radio']").click()
