"""."""

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)

driver.maximize_window()
driver.get('http://demo.guru99.com/test/drag_drop.html')

# driver.switch_to.frame(0)

des = driver.find_element_by_xpath("//*[@id='credit2']/a")
man = driver.find_element_by_xpath("//*[@id='bank']/li")
action = ActionChains(driver)

# move element by x,y coordinates on the screen
action.move_to_element(des)
action.drag_and_drop(man, des)
action.perform()
