#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://localhost:5000/form_html5_progress")

# click radio button
progress = driver.find_element_by_tag_name("progress")


print(progress.text)
print(progress.value_of_css_property("width"))
print(progress.get_attribute("value"))
