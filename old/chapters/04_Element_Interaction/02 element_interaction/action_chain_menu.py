#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver

from selenium.webdriver import ActionChains


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/bootstrap_menu")

menu = driver.find_element_by_css_selector(".nav-item.dropdown")
hidden_submenu = driver.find_element_by_css_selector(" #link2")

actions = ActionChains(driver)
actions.move_to_element(menu)
actions.click(menu)
actions.click(hidden_submenu)
actions.perform()
