#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://localhost:5000/form_bootstrap_radiobutton")

ele = driver.find_elements_by_css_selector("[type='radio']")

print(ele)
e = ele[1]
for a in dir(e):
    try:
        print(a, eval("e.{a}()".format(a=a)))
    except:
        print("e.{a}".format(a=a))
