"""."""

from selenium import webdriver


driver = webdriver.Opera()

driver.maximize_window()
driver.get('http://localhost:5000/cookies')

print(driver.get_cookie("nationalist"))
new_cookies = {"name": "Author", "value": "Mayank Johri"}
driver.add_cookie(new_cookies)
print("~" * 20)

for c in driver.get_cookies():
    print(c['name'], ":", c['value'])

print("^" * 20)
driver.delete_cookie("")
for c in driver.get_cookies():
    print(c['name'], ":", c['value'])

for c in driver.get_cookies():
    print(c['name'], ":", c['value'])


driver.close()
