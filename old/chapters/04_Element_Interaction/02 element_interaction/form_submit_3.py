#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_2")

# click radio button
cb = driver.find_element_by_css_selector("input[type='checkbox']")
cb.click()
ele = driver.find_element_by_css_selector("button[type='submit']")
ele.submit()
