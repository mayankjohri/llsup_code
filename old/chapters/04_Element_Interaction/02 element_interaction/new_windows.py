#!/usr/bin/env python
# coding=utf-8

"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
browser = webdriver.Chrome(chrome_options=options)

browser.get('https://www.google.com?q=python#q=python')
first_result = ui.WebDriverWait(browser, 15).until(lambda browser: browser.find_element_by_class_name('rc'))
first_link = first_result.find_element_by_tag_name('a')

# Save the window opener (current window)
main_window = browser.current_window_handle

# Open the link in a new window by sending key strokes on the element
first_link.send_keys(Keys.SHIFT + Keys.RETURN)

# Get windows list and put focus on new window (which is on the 1st index in the list)
windows = browser.window_handles
browser.switch_to.window(windows[1])

# do whatever you have to do on this page, we will just got to sleep for now
sleep(2)

# Close current window
browser.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 'w')

# Put focus back on main window
browser.switch_to.window(main_window)
