#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

driver.implicitly_wait(1)

try:
    not_found = driver.find_element_by_id("not_found")
except Exception as e:
    print(e)
