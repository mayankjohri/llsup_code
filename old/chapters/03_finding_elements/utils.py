#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 16:46:07 2018

@author: mayank
"""


def is_element_present(wd, css_selector):
    """."""
    try:
        wd.find_element_by_css_selector(css_selector)
        return True
    except Exception as ex:
        return False
