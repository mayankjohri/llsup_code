#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from utils import is_element_present


BIN = FirefoxBinary('/opt/firefox/firefox')
WD = webdriver.Firefox(firefox_binary=BIN)
WD.get("https://www.reddit.com/")

c = {
     "dob": "#desktop-onboarding-browse",
     "skip": ".skip-for-now",
     "signup": ".listingsignupbar__cta-desc",
     "remember_me": "#rem-login-main"
    }

if is_element_present(WD, c["dob"]):
    WD.find_element_by_css_selector(c["skip"]).click()

ele = WD.find_element_by_css_selector(c['remember_me'])
ele.click()
