#!/usr/bin/env python
# coding=utf-8
#!/usr/bin/env python
import unittest
from selenium import webdriver


class TestUbuntuHomepage(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.PhantomJS('/home/mayank/apps/web_drivers/phantomjs')


    def testTitle(self):
        self.driver.get('http://www.ubuntu.com/')
        self.assertIn('Ubuntu', self.driver.title)


    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main(verbosity=2)

