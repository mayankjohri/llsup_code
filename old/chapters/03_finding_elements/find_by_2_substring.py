#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


BIN = FirefoxBinary('/opt/firefox/firefox')
WD = webdriver.Firefox(firefox_binary=BIN)
WD.get("http://127.0.0.1:5000/substr")

#########################################################
# It should not work as :contains is not a css 3 selector
#########################################################
new_post = WD.find_element_by_css_selector("div:contains(\"20\")")
print(new_post.text)
