#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 15:57:43 2018

@author: mayank
"""
from flask import Flask

app = Flask(__name__)


@app.route('/substr')
def entry_point():
    return """<html>
<body>
    <ul>
      <li data-years="1800-1900">The 19th Century</li>
      <li data-years="1900-2000">The 20th Century</li>
      <li data-years="2000-2100">The 21st Century</li>
    </ul>
</body>
</html>"""


if __name__ == '__main__':
    app.run(debug=True)
