#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/countdown")
    try:
        element = WebDriverWait(driver, 15).until(
            ec.presence_of_all_elements_located((By.CLASS_NAME, "num"))
        )
    except TimeoutException as e:
        print("Error: Sorry was not able to find the element within required time")
    print("Done")
