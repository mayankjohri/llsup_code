#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/countdown")
    try:
        element = WebDriverWait(driver, 15).until(
            ec.presence_of_element_located((By.ID, "swagatam"))
        )
    except Exception as e:
        print(e)
    print("Done")
