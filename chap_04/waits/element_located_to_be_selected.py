#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from time import sleep

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/")
    css_val = "#exampleSelect2 > option[value='2']"
    WebDriverWait(driver, 15).until(
        ec.element_located_to_be_selected((By.CSS_SELECTOR, css_val))
    )
    sleep(10)
    print("Done")
