#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from time import sleep

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/")
    css_val = "#exampleSelect2 > option[value='2']"
    ele = driver.find_element_by_css_selector(css_val)
    WebDriverWait(driver, 15).until(
        ec.element_selection_state_to_be(ele, True)
    )
    sleep(10)
    print("Done")
