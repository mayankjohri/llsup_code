#!/usr/bin/env python
# coding=utf-8
# Use it if you are sure about element exists.

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/countdown")
    ele = driver.find_element_by_id("msg")
    element = WebDriverWait(driver, 15).until(
        ec.visibility_of(ele)
    )
    print("Done")
