#!/usr/bin/env python
# coding=utf-8

# to be used on input type with "value" attribute

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/countdown")
    element = WebDriverWait(driver, 15).until(
        ec.element_to_be_clickable((By.ID, "txt_no"))
    )
    print("Done")
