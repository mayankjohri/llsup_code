#!/usr/bin/env python
# coding=utf-8
# It will wait till the element is alive, once its deleted wait will be over.

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/timeout")
    ele = driver.find_element_by_id("ohm")
    element = WebDriverWait(driver, 15).until(
        ec.staleness_of(ele)
    )
    print("Done")
