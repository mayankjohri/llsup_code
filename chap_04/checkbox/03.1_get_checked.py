#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver


def get_checked(driver, css):
    ele = driver.find_elements_by_css_selector('input[class="badgebox"]')
    for  e in ele:
        if e.get_attribute("checked"):
            yield e

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_cb_badge")
    for e in get_checked(driver, 'input[class="badgebox"]'):
        print(e.get_attribute('id'))

