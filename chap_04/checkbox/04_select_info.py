#!/usr/bin/env python
# coding=utf-8
# chap_03/finding_elements/2_find_element_by_css_selector.py

from selenium import webdriver
from time import sleep

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_cb_badge")
    ele = driver.find_element_by_css_selector('label[for="info"]')
    #  will not work :(
    ele.click()
    sleep(10)

