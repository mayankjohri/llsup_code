#!/usr/bin/env python
# coding=utf-8
# chap_03/finding_elements/2_find_element_by_css_selector.py

from selenium import webdriver

def get_checked(driver, css):
    css = css + '[checked=""]'
    return driver.find_elements_by_css_selector(css)

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_cb_badge")
    ele = get_checked(driver, 'input[class="badgebox" ]')
    for  e in ele:
        print(e.get_attribute("id"))

