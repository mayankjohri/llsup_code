#!/usr/bin/env python
# coding=utf-8


from selenium import webdriver
from time import sleep


with webdriver.Firefox() as driver:
    driver.get('https://localhost:4040/cookies')
    sleep(5)
    print(driver.get_cookie("nationalist"))

    print("~"*20)
    new_cookies = {"name": "Author", "value": "Mayank Johri"}
    driver.add_cookie(new_cookies)
    new_cookies = {'name': 'Visonary', 'path': '/', 'secure': True, 'value': 'Avul Pakir Jainulabdeen Abdul Kalam'}
    driver.add_cookie(new_cookies)
    for c in driver.get_cookies():
      print(c['name'], ":", c['value'], ":", c['secure'])
