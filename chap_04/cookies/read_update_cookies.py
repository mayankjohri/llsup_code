#!/usr/bin/env python
# coding=utf-8


from selenium import webdriver
from time import sleep


with webdriver.Firefox() as driver:
    driver.get('http://localhost:5000/cookies')
    sleep(5)
    print(driver.get_cookie("nationalist"))
    for c in driver.get_cookies():
        print(c['name'], ":", c['value'])
    print("~"*20)
    new_cookies = {"name": "Author", "value": "Mayank Johri"}
    driver.add_cookie(new_cookies)

    for c in driver.get_cookies():
        print(c['name'], ":", c['value'])
