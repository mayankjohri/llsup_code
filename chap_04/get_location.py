#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.common.by import By


with webdriver.Firefox() as driver:
  driver.get("http://127.0.0.1:5000/")
  elements = [
          (By.NAME, "option_two"),
          (By.CSS_SELECTOR, "input[value='Submit']")
          ]
  for by_type, value in elements:
    ele = driver.find_element(by_type, value)
    print(ele.location)

