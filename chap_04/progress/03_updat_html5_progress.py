#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/progress")
    ele = driver.find_element_by_id("file")
    print(ele.text)
    print(ele.get_attribute("max"))
    print(ele.get_attribute("value"))
