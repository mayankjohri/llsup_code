#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.common.by import By

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_bootstrap_progress")
    progresses = driver.find_elements(By.CLASS_NAME, "progress")

    for prog in progresses:
        for e in ['progress-type', "progress-completed", "sr-only"]:
            print(prog.find_element(By.CLASS_NAME, e).text)
        print(prog.value_of_css_property("width"))
