#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.common.by import By

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/progress")
    ele = driver.find_element(By.ID, "file")

    print(ele.text)
    for attr in "max", "value":
       print(ele.get_attribute(attr))
