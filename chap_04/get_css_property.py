#!/usr/bin/env python
# coding=utf-8
# chap_03/finding_elements/2_find_element_by_css_selector.py

from selenium import webdriver
from selenium.webdriver.common.by import By

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/")
    props = ["display",
             "box-sizing",
             "color",
             "font-size",
             "font-family",
             "font-style",
             "text-align"]

    ele = driver.find_element(By.CSS_SELECTOR, "#mango")
    for prop in props:
        print(f"{prop = }, { ele.value_of_css_property(prop) = }")
