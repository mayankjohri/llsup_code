#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/")
    sleep(10)
    ele = driver.find_element(By.ID, "pineapple")
    ele.click()
    sleep(10)
    ele = driver.find_element(By.CSS_SELECTOR, 'button[type="submit"]')
    ele.click()
    sleep(10)
