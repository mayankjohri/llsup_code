#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.by import By

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_cb_badge")

    ele = driver.find_elements(By.CSS_SELECTOR, "[type='checkbox']")
    for e in ele:
        print(e.get_attribute('id'), e.is_selected())
