#!/usr/bin/env python
# coding=utf-8
from time import sleep

from selenium import webdriver
from selenium.webdriver.support.select import Select
from  selenium.webdriver.common.by import By


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

    ele = driver.find_element(By.ID, "cleanest")
    select = Select(ele)
    # I am not providing the ID but text which is displayed
    #
    sleep(10)
    select.select_by_value("Tirupati")
    sleep(10)
