#!/usr/bin/env python
# coding=utf-8
from time import sleep

from selenium import webdriver
from  selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

    ele = driver.find_element(By.ID, "cleanest")
    select = Select(ele)
    # I am not providing the ID but text which is displayed
    select.select_by_index(3)

    for element in select.all_selected_options:
        print(element.get_attribute("id"))
