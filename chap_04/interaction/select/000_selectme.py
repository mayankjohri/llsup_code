#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.support.select import Select

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

    ele = driver.find_element_by_id("cleanest")
    select = Select(ele)
    print(dir(select))
