#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.support.select import Select
from  selenium.webdriver.common.by import By

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

    ele = driver.find_element(By.ID, "cleanest")
    select = Select(ele)
    print(select.is_multiple)

