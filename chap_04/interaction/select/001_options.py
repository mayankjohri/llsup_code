#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.support.select import Select
from  selenium.webdriver.common.by import By
from time import sleep

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

    ele = driver.find_element(By.ID, "cleanest")
    select = Select(ele)

    for element in select.options:
        print(element.get_attribute("id") , " ==> ", element.text,
              f" {element.get_attribute('value') = }")

    print(dir(element))
