from time import sleep

from selenium import webdriver
from selenium.webdriver.common.by import By

options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
with webdriver.Chrome(options=options) as driver:
    driver.get("http://127.0.0.1:5000/send_clear")

    # click radio button
    ele = driver.find_element(By.ID, "example-text-input")
    ele.send_keys(" >>> Testing is always good. ~1@#$%^&*)_+")
    sleep(5)
