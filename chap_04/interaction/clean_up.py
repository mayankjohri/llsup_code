#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/send_clear")
    data = {
        "example-text-input": "test",
        "example-search-input": "Mayank Johri",
        "example-email-input": "Mayank Johri",
        "example-tel-input": "Mayank Johri",
        "example-password-input": "test",
        "example-number-input": "tese",
        "example-number-input": "tese",
        "example-datetime-local-input": "Mayank",
        "example-date-input": "Mayank",
        "example-month-input": "Mayank",
        "example-week-input": "Mayank"
    }
    print(f"{driver.current_url = }")
    print(f"{driver.title = }")
    sleep(5)
    driver.refresh()
    sleep(5)
    for key, _ in data.items():
        ele = driver.find_element(By.ID, key)
        ele.clear()
    driver.save_screenshot("{filename}.png".format(filename=__file__))
