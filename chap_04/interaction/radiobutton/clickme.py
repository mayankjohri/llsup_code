#!/usr/bin/env python
# coding=utf-8
from time import sleep
from selenium import webdriver
from selenium.webdriver.support.select import Select

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_bootstrap_radiobutton")

    ele = driver.find_element_by_css_selector("input[name='options'][id='primary']")
    ele.click()
    sleep(10)

