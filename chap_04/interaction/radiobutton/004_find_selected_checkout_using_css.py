#!/usr/bin/env python
# coding=utf-8
from time import sleep

from selenium import webdriver
from selenium.webdriver.support.select import Select
from  selenium.webdriver.common.by import By

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_bootstrap_radiobutton")
    for selector in ("options", "options_one"):
        try:
            print(f"Using {selector = }")
            ele = driver.find_element(By.CSS_SELECTOR,
                    f"input[name='{selector}']:checked")
            print(ele.get_attribute('id'), ele.is_selected())
        except Exception as e:
            print(f"Error: {e}")

