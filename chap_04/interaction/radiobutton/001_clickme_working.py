#!/usr/bin/env python
# coding=utf-8
from time import sleep
from selenium import webdriver
from  selenium.webdriver.common.by import By

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_bootstrap_radiobutton")

    ele = driver.find_element(By.CSS_SELECTOR,
                              "input[name='options'][id='primary'] ~ span")
    ele.click()
    sleep(4)

    ele = driver.find_element(By.CSS_SELECTOR,
                              "input[name='options'][id='info'] ~ span")
    ele.click()
    sleep(5)

    # Common mistake of directly clicking the radio button, it will not work
    ele = driver.find_element(By.CSS_SELECTOR,
                              "input[name='options'][id='default']")
    ele.click()
    sleep(10)
