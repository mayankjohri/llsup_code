#!/usr/bin/env python
# coding=utf-8
from time import sleep

from selenium import webdriver
from selenium.webdriver.support.select import Select
from  selenium.webdriver.common.by import By

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/form_bootstrap_radiobutton")

    ele = driver.find_elements(By.CSS_SELECTOR, "[type='radio']")
    for e in ele:
        print(e.get_attribute('id'), e.is_selected())

    sleep(10)
