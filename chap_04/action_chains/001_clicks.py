#!/usr/bin/env python
# coding=utf-8
"""."""
from time import sleep

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/bootstrap_menu")

    menu = driver.find_element(By.CSS_SELECTOR, ".nav-item.dropdown")
    hidden_submenu = driver.find_element(By.CSS_SELECTOR, " #link2")

    actions = ActionChains(driver)
    actions.click(menu).pause(4)
    actions.click(hidden_submenu)
    print("Lets Start the action")
    sleep(3)
    actions.perform()
    print("All done")
    sleep(5)
    print("Lets exit")
