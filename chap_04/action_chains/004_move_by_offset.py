#!/usr/bin/env python
# coding=utf-8
"""."""
from time import sleep

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By



with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/canvas")
    canvas = driver.find_element(By.ID, "canvas")
    actions = ActionChains(driver)
    actions.move_to_element(canvas)
    actions.move_by_offset(50, 20).click().pause(2).move_by_offset(50, 30)
    actions.pause(2).move_by_offset(50, -50)
    actions.pause(2).move_by_offset(50, 50)
    actions.pause(2).move_by_offset(50, -50)
    actions.pause(2).move_by_offset(50, 50)
    actions.pause(2).move_by_offset(50, -50)
    actions.pause(2).move_by_offset(50, 50)
    actions.perform()
    sleep(5)
