#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from time import sleep

with webdriver.Firefox() as driver:
    driver.get("http://localhost:5000/drag_and_drop")

    des = driver.find_element(By.ID, "droppable")
    man = driver.find_element(By.ID, "draggable")
    actions = ActionChains(driver)
    actions.drag_and_drop(man, des)
    actions.perform()
    sleep(5)
