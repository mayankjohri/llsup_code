#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver import ActionChains

from time import sleep

with webdriver.Firefox() as driver:
    driver.get("http://localhost:5000/drags")


    man = driver.find_element_by_id("draggable")
    actions = ActionChains(driver)
    # actions.move_to_element(bx)
    actions.drag_and_drop_by_offset(man, 10, 20).pause(0.5)
    actions.drag_and_drop_by_offset(man, 40, 40).pause(0.5)
    actions.drag_and_drop_by_offset(man, 180, 40).pause(0.5)
    actions.drag_and_drop_by_offset(man, -180, 40).pause(0.5)
    actions.drag_and_drop_by_offset(man, -40, -40).pause(0.5)
    actions.perform()
    sleep(5)
