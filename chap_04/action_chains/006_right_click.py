#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver import ActionChains
from time import sleep

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/context")
    link = driver.find_element_by_id("clickme")
    cut = driver.find_element_by_class_name("context-menu-icon-cut")
    actions = ActionChains(driver)
    actions.move_to_element(link)
    actions.context_click(link)
    actions.click(cut)
    actions.perform()
    sleep(10)
    driver.quit()
