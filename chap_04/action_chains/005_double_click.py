#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver import ActionChains

from time import sleep

with webdriver.Firefox() as driver:
    driver.get("http://localhost:5000/clicks")

    des = driver.find_element_by_id("clickme")
    actions = ActionChains(driver)
    actions.click(des).click(des).pause(5)
    actions.click(des).pause(3)
    actions.double_click(des)
    actions.perform()
    sleep(5)
