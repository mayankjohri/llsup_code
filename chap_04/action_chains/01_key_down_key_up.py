#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains

from time import sleep


def open_url(driver, url):
    JS_SCRIPT = 'window.open("{}", "_blank");'
    driver.execute_script(JS_SCRIPT.format(url))

with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/bulma")
    open_url(driver, "http://127.0.0.1:5000/success")
    open_url(driver, "http://127.0.0.1:5000/form_1")
    actions = ActionChains(driver)
    # actions.move_to_element(bx)
    driver.find_element_by_tag_name('html').send_keys(Keys.CONTROL + Keys.TAB)
    sleep(3)
    driver.find_element_by_tag_name('html').send_keys(Keys.CONTROL + Keys.TAB)
    sleep(5)
    actions = ActionChains(driver)
    actions.key_down(Keys.CONTROL).key_down(Keys.TAB).key_up(Keys.TAB)
    actions.key_up(Keys.CONTROL).perform()
    sleep(5)
    driver.switch_to_window(driver.window_handles[2])
    sleep(5)
