#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.common.by import By


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/")
    ele = driver.find_element(By.ID, "pineapple")
    print(ele.get_attribute("id"))
    print(ele.get_attribute("text"))
    print(ele.get_attribute("disabled"))

    print("Lets get details of banana....")
    ele = driver.find_element(By.ID, "banana")
    print(ele.get_attribute("id"))
    print(ele.get_attribute("text"))
    print(ele.get_attribute("disabled"))
