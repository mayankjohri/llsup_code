#!/usr/bin/env python
# coding=utf-8
# chap_03/finding_elements/2_find_element_by_css_selector.py

from selenium import webdriver


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/")
    props = ["display", "id", "class", 'text',
             "color", "box-sizing",
             "font-size", "font-stretch", "selected",  "checked"]
    ele = driver.find_element_by_css_selector("#exampleTextarea")
    for prop in props:
        print(prop, ele.get_property(prop), ele.get_attribute(prop))
