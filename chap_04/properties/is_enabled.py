#!/usr/bin/env python
# coding=utf-8
# chap_03/finding_elements/2_find_element_by_css_selector.py

from selenium import webdriver


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/")
    css_selectors = [
        'ul a[href="#home"]',
        'li > a[href="#home"]',
        '#about',
        'button.btn-primary',
        'input[disabled]']
    for css in css_selectors:
        ele = driver.find_element_by_css_selector(css)
        print("Element with css `{}` is:".format(css))
        print("Enabled: {}".format(ele.is_enabled()))
        print("Displayed: {}".format(ele.is_displayed()))
        print("Selected: {}".format(ele.is_selected()))

