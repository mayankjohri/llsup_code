#!/usr/bin/env python
# coding=utf-8
# chap_03/finding_elements/2_find_element_by_css_selector.py

from selenium import webdriver


with webdriver.Firefox() as driver:
    driver.get("http://127.0.0.1:5000/")
    props = ["display", "color", "font-size", "font-style", "text-align"]
    ele = driver.find_element_by_css_selector("#mango")
    for prop in props:
        print(prop, ele.value_of_css_property(prop))
