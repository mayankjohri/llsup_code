#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver


with webdriver.Firefox() as driver:
  driver.get("http://127.0.0.1:5000/")
  ele = driver.find_element_by_id("pineapple")
  print(ele.get_attribute("id"))
  ele = driver.find_element_by_id("banana")
  print(ele.get_attribute("text"))
  print(ele.get_attribute("disabled"))
