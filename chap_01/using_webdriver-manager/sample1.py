from selenium import webdriver
from selenium.webdriver.firefox.service import Service as FirefoxService
from webdriver_manager.firefox import GeckoDriverManager


from webdriver_manager.core.utils import read_version_from_cmd, PATTERN
version = read_version_from_cmd("~/Apps/browsers/firefox/firefox-bin --version", PATTERN["firefox"])
driver_binary = GeckoDriverManager(version=version).install()
